require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, el| acc += el }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |el| el.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  counter = Hash.new(0)
  arr = string.gsub(" ", "").split('')
  arr.each { |char| counter[char] += 1 }
  counter.select { |k, v| v > 1 }.keys.to_a
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  arr = string.gsub(/[^a-z0-9\s]/i, '')
  arr = arr.split(' ')
  new_arr = [arr[0], arr[1]]
  arr.each do |word|
     if word.length > new_arr[1].length
       new_arr[0] = new_arr[1]
       new_arr[1] = word
     elsif
       word.length > new_arr[0].length
       new_arr[0] = word
     else
     end
  end
  new_arr
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alpha = ('a'..'z').to_a
  alpha.delete(string).split('')
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  result = []
  year = first_yr
  until year > last_yr
    result << year if not_repeat_year?(year)
    year += 1
  end
  result
end

def not_repeat_year?(year)
  year = year.to_s.split('')
  year.each_with_index do |char_one, idx|
    year[idx+1..-1].each { |char_two| return false if char_one == char_two }
  end
  return true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  arr = string.gsub(".", "").split(' ')
  result = arr[0]
  arr[1..-1].each { |word| result = word if c_distance(word) < c_distance(result) }
  result
end

def c_distance(word)
  return 10 if word.include?("c") == false
  word.chars.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  new_arr = arr.dup
  nums_to_check = arr.select { |num| num_repeat?(arr, num) }.uniq
  nums_to_check.each { |num| result << [new_arr.index(num), new_arr.length - new_arr.reverse.index(num)- 1] }
  result
end

def num_repeat?(arr, num)
  arr.select { |el| el == num}.length >= 2 }
end
